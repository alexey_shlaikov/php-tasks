<?php

class View
{
    /**
     * @var error Error of rendered views.
     */
    private $_error = null;

    private $data = array();

    private $render = FALSE;


    /**
     * Initialize a new view context.
     * @param $template
     */
    public function __construct($template)
    {
        try {
            $file = APP_DIR . '/views/' . strtolower($template) . '.php';

            if (file_exists($file)) {
                $this->render = $file;
            } else {
                throw new Exception('Template ' . $template . ' not found!');
            }
        }
        catch (Exception $_error) {
            $this->_setError('Шаблон не найден.');
        }
    }


    /**
     * Add a new data to render php template.
     * @param $variable
     * @param $value
     */
    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }


    /**
     * @param $error
     */
    private function _setError($error)
    {
        $this->_error = $error;
    }


    /**
     * @return null
     */
    public function getError()
    {
        return $this->_error;
    }


    public function __destruct()
    {
        extract($this->data);
        include($this->render);
    }

}