<?php

require 'simple_html_dom.php';


class Requests
{
	/**
	 * @var error Error of requests.
	 */
    private $_error = null;


    /**
     * Return a format string of date and time.
     * @param $format of DateTime
     * @return string with DateTime
     * @internal param $request
     */
    public function getTimeDate($format)
    {
        return date($format);
    }


    /**
     * Parse DOM->Title element from website. Regexes with supplied parameters
     * @param $url
     * @return string
     */
    private function _getPageTitle($url)
    {
        $str = file_get_contents($url);

        if(strlen($str) > 0) {
            $str = trim(preg_replace('/\s+/', ' ', $str));
            preg_match("/\<title\>(.*)\<\/title\>/i", $str, $title);

            return strval($title[1]);
        }
        else {
            $this->_setError('404');

            return '';
        }
    }


    /**
     * Curl method to connect with any options.
     * @param $url
     * @return mixed HTML page
     */
    private function _openUrl($url)
    {
        $ch= curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_VERBOSE,0);
        curl_setopt($ch, CURLOPT_COOKIESESSION, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_REFERER,'https://www.google.ru/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $htmlContent= curl_exec($ch);
        curl_close ($ch);

        return($htmlContent);
    }


    /**
     * Scrapping first string in Yandex search.
     * @param $text
     * @return string return LINK
     */
    private function _scrapLinkYandex($text)
    {
        $url = 'https://yandex.ru';
        $html = str_get_html($this->_openUrl($url .'/search/?text='. $text .'&lr=2'));
        $link = $html->find('a[class=serp-item__title-link]', 0)->href;

        return strval($link);
    }


    /**
     * Return a JSON format string
     * @param $request 'GET' request matches.
     * @return string
     */
    public function getJSONData($request)
    {
        $url = iconv('UTF-8', 'windows-1251', $request);

        $jsonArray = array(
            'data' => $this->_getPageTitle($this->_scrapLinkYandex($url)),

        );

        return json_encode($jsonArray, JSON_UNESCAPED_UNICODE);
    }


    /**
     * Return a XML format string
     * @param $request
     * @return string
     */
    public function getXMLData($request)
    {
        if (filter_var($request, FILTER_VALIDATE_EMAIL)) {
            $node = 'content';
            $key = $request;

            return $this->_sendXMLResponse($node, $key);
        }
        else {
            $node = 'error';
            $key = 'email error';
            $this->_setError('valid email');

            return $this->_sendXMLResponse($node, $key);
        }
    }


    /**
     * Sending XML response to client.
     * @param $node main node.
     * @param $key in child of node
     * @return string
     */
    private function _sendXMLResponse($node, $key)
    {
        $response = '<?xml version="1.0" encoding="utf-8"?>';
        $response .= '<'. strval($node) .'>'. strval($key) .'</'. strval($node) .'>';

        return $response;
    }


    /**
     * @param $error
     */
    private function _setError($error)
    {
        $this->_error = $error;
    }


    /**
     * @return null
     */
    public function getError()
    {
        return $this->_error;
    }

}