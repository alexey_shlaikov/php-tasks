<?php

mb_internal_encoding("UTF-8");
define("APP_DIR", __DIR__);


require_once APP_DIR . '/vendor/autoload.php';
require_once 'classes/Requests.php';
require_once 'classes/Router.php';
require_once 'classes/View.php';


$controller = new Router();

$controller->map( 'GET', '/', function ()
{
    echo 'Домашняя работа';
}, 'index');

$controller->map( 'GET', '/[:anything]', function ($anything) {
    $engine = new Requests();
    $view = new View('timedate');
    $view->assign('request', urldecode($anything));
    $view->assign('date', $engine->getTimeDate('Y-m-d H:i:s'));

    return $view;
}, 'anything');

$controller->map( 'GET', '/json/[*:anything]', function ($anything)
{
    $engine = new Requests();
    echo $engine->getJSONData($anything);
}, 'json');

$controller->map( 'GET', '/xml/[*:email]', function ($email)
{
    $engine = new Requests();
    header("Content-type: text/xml; charset=utf-8");
    echo $engine->getXMLData(urldecode($email));
}, 'xml');


$match = $controller->match();

if ($match && is_callable( $match['target'])) {
    call_user_func_array( $match['target'], $match['params'] );
} else {
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    echo ' Ошибка 404: Страница не найдена ';
}