<?php

mb_internal_encoding("UTF-8");
require_once './vendor/autoload.php';
require_once('./classes/Requests.php');

use \PHPUnit\Framework\TestCase;


class RequestsTest extends TestCase
{

    public function testFailingInclude()
    {
        include './vendor/autoload.php';
    }


    public function testYandexParsing()
    {
        $this->assertEquals(
            array(
                $this->_getFromJSON('Python'),
                $this->_getFromJSON('anything'),
                $this->_getFromJSON('Yandex'),
                $this->_getFromJSON('money'),
            ),
            array(
                'Python — официальный сайт',
                'Anything: перевод, произношение, транскрипция...',
                'Яндекс — поисковая система и интернет-портал',
                'Яндекс.Деньги — сервис онлайн-платежей',
            ));
    }


    public function testDateTimeResponse()
    {
        $engine = new Requests();

        $expected = $engine->getTimeDate('31-01-1995', 'Y-m-d H:i:s');
        $actual = '1995-01-31 00:00:00';

        $this->assertEquals($expected, $actual);
    }


    private function _getFromJSON($request){
        $engine = new Requests();
        $key = 'yandex_ru';
        $data = json_decode($engine->getJSONData($request), true);

        return $data[$key];
    }

}
